# convert-gpx-file-to-svg

[Try to respect Karma conventions for git commit Msg please](http://karma-runner.github.io/2.0/dev/git-commit-msg.html)

## Getting Started

Install Node.js and npm if they are not already on your machine.

Clone this repo into new project folder

Install the npm packages described in the package.json

```shell
npm install
```

[See demo](http://cecilebruzac.fr/convert-gpx-file-to-svg/)