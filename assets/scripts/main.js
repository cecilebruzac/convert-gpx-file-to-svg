let map = null;

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 0, lng: 0},
    zoom: 0,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    draggable: false,
    disableDefaultUI: true
  });

  drawFromGpxFile('Move_2016_08_16_17_49_52_Cycling.gpx', map);
}


function ptsFromGpx(data) {
  const trkpts = data.getElementsByTagName('trkpt');
  let pts = [];

  for (let i = 0; i < trkpts.length; i++) {
    const lat = trkpts[i].getAttribute('lat');
    const lng = trkpts[i].getAttribute('lon');
    pts[i] = {lat: parseFloat(lat), lng: parseFloat(lng)};
  }

  return pts;
}

function getPtsFromGpxFile(path) {
  return new Promise(function (resolve, reject) {
    let pts = [];
    const req = new XMLHttpRequest();

    req.open('GET', path, true);

    req.onreadystatechange = function (evt) {
      if (req.readyState === 4 && req.status === 200) {
        const gpx = req.responseXML;
        pts = ptsFromGpx(gpx);

        if (pts.length > 0) {
          resolve(pts);
        } else {
          reject(Error('Error'));
        }
      }

      if (req.readyState === 4 && req.status !== 200) {
        reject(Error('Error'));
      }
    };

    req.onerror = function () {
      reject(Error('Error'));
    };

    req.send(null);
  });
}

function projectedCoordsInTheDrawing(latLng, map) {
  const topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
  const bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
  const scale = Math.pow(2, map.getZoom());
  const worldPt = map.getProjection().fromLatLngToPoint(latLng);
  return new google.maps.Point((worldPt.x - bottomLeft.x) * scale, (worldPt.y - topRight.y) * scale);
}

function saveSvg(svg) {
  const tmp = document.createElement('div');
  tmp.appendChild(svg.cloneNode(true));
  const req = new XMLHttpRequest();
  const filePath = 'generated/track_' + Date.now() + '.svg';
  req.open('POST', 'phpwritesvg.php', true);
  req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  req.send('svg=' + tmp.innerHTML + '&filePath=' + filePath);

  req.onreadystatechange = function () {
    if (req.readyState === 4 && req.status === 200) {
      const prevImg = document.getElementById('img');

      if (prevImg) {
        prevImg.parentNode.removeChild(prevImg);
      }

      const img = document.createElement('img');
      img.setAttribute('src', filePath);
      img.setAttribute('id', 'img');

      document.body.insertBefore(img, document.getElementById('map'));
    }
  }
}

function downloadSvg(svg) {
  const tmp = document.createElement('div');
  tmp.appendChild(svg.cloneNode(true));

  const blob = new Blob([tmp.innerHTML], {type: 'application/svg+xml'});
  saveAs(blob, 'drawing_' + Date.now() + '.svg');
}

function drawInsideSVG(pts, map) {
  const prevSvg = document.getElementById('svg');
  if (prevSvg) {
    prevSvg.parentNode.removeChild(prevSvg);
  }

  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
  svg.setAttribute('x', '0');
  svg.setAttribute('y', '0');
  svg.setAttribute('width', '1280px');
  svg.setAttribute('height', '800px');
  svg.setAttribute('viewBox', '0 0 1280 800');
  svg.setAttribute('id', 'svg');

  const polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
  polyline.setAttribute('fill', 'none');
  polyline.setAttribute('stroke', '#000000');

  let ptToDraw = null;
  let ptsToDraw = '';
  for (let i = 0; i < pts.length; i++) {
    ptToDraw = projectedCoordsInTheDrawing(new google.maps.LatLng(pts[i].lat, pts[i].lng), map);
    ptsToDraw += ptToDraw.x + ',' + ptToDraw.y + ' ';
  }

  polyline.setAttribute('points', ptsToDraw);
  svg.appendChild(polyline);
  document.body.insertBefore(svg, document.getElementById('map'));

  saveSvg(svg);
  downloadSvg(svg);
}


function drawInsideCanvas(pts, map) {

  const prevCanvas = document.getElementById('canvas');
  if (prevCanvas) {
    prevCanvas.parentNode.removeChild(prevCanvas);
  }

  const canvas = document.createElement('canvas');
  canvas.setAttribute('class', 'canvas');
  canvas.setAttribute('height', '1600');
  canvas.setAttribute('width', '2560');
  canvas.setAttribute('id', 'canvas');

  const ctx = canvas.getContext('2d');

  ctx.scale(2, 2);

  ctx.beginPath();
  ctx.lineWidth = '1';
  ctx.strokeStyle = '#0000FF';

  let ptToDraw, lastDrawnPt = null;

  for (let i = 0; i < pts.length; i++) {
    ptToDraw = projectedCoordsInTheDrawing(new google.maps.LatLng(pts[i].lat, pts[i].lng), map);
    if (lastDrawnPt) {
      ctx.moveTo(lastDrawnPt.x, lastDrawnPt.y);
      ctx.lineTo(ptToDraw.x, ptToDraw.y);
    }
    lastDrawnPt = ptToDraw;
  }
  ctx.stroke();

  document.body.insertBefore(canvas, document.getElementById('map'));
}

function drawOnMap(pts, map) {
  if (map) {
    const bounds = new google.maps.LatLngBounds();
    for (let i = 0; i < pts.length; i++) {
      bounds.extend(pts[i]);
    }
    map.fitBounds(bounds);

    google.maps.event.addListenerOnce(map, 'bounds_changed', function () {
      drawInsideCanvas(pts, map);
      drawInsideSVG(pts, map);
    });

    const trk = new google.maps.Polyline({
      path: pts,
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 1
    });

    trk.setMap(map);
  }
}

function drawFromGpxFile(path, map) {
  getPtsFromGpxFile(path)
    .then(function (pts) {
      drawOnMap(pts, map);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function drawOnGpxFileSelect() {
  const selectedGpxFile = this.files[0];
  const reader = new FileReader();
  reader.onload = function (event) {
    const gpx = (new window.DOMParser()).parseFromString(reader.result, 'text/xml');
    const pts = ptsFromGpx(gpx);
    drawOnMap(pts, map);
  };
  reader.readAsText(selectedGpxFile);
}


(function () {
  const gpxFileInputElement = document.getElementById('gpx-file-input');
  gpxFileInputElement.addEventListener('change', drawOnGpxFileSelect, false);
})();