<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Convert gpx file to svg</title>
  <meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="assets/styles/main.css">
</head>

<body class="row">
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
  your browser</a> to improve your experience and security.</p>
<![endif]-->

  <div class="ui">
    <label for="gpx-file-input">Select a new .gpx file to convert to svg</label>
    <br>
    <input type="file"
           id="gpx-file-input" name="gpx-file-input"
           accept=".gpx">
  </div>
  <div id="map"
       class="map"></div>

<script src="assets/scripts/main.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQ_VIxoevprRoXyiaHb1woF5ISuaPF2UY&callback=initMap"></script>
<script src="node_modules/file-saver/dist/FileSaver.min.js"></script>

</body>
</html>